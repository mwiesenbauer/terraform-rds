variable "cidr_block" {
  description = "CIDR block for custom VPC."
  type        = string
}

variable "db_instance_identifier" {
  description = "RDS instance identifier."
  type        = string
  default     = "db-ambos"
}

variable "db_engine_version" {
  description = "RDS engine version."
  type        = string
  default     = "11.6"
}

variable "db_instance_size" {
  description = "RDS instance type."
  type        = string
  default     = "db.t2.micro"
}

variable "db_instance_storage" {
  description = "RDS instance storage."
  type        = number
  default     = 20
}

variable "db_instance_name" {
  description = "RDS instance pretty name."
  type        = string
  default     = "ambos"
}

variable "db_instance_username" {
  description = "RDS instance management user."
  type        = string
  default     = "admin_ambos"
}

variable "db_instance_password" {
  description = "RDS instance management user password."
  type        = string
  # FIXME: set unused password that fullfills RDS password constraints
  default   = ""
  sensitive = true
}

variable "db_multi_az" {
  description = "RDS multi AZ setup."
  type        = bool
  default     = true
}

variable "db_backup_retention_period" {
  description = "RDS maximum backup retention time span in days."
  type        = number
  default     = 10
}

variable "db_backup_tag_value" {
  description = "Tag value use for backup field, used by AWS Backup plan."
  type        = string
  default     = "no"
}