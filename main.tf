locals {
  subnets_backend = {
    eu-central-1a = cidrsubnet(var.cidr_block, 4, 1)
    eu-central-1b = cidrsubnet(var.cidr_block, 4, 2)
    eu-central-1c = cidrsubnet(var.cidr_block, 4, 3)
  }

  subnets_db = {
    eu-central-1a = cidrsubnet(var.cidr_block, 4, 11)
    eu-central-1b = cidrsubnet(var.cidr_block, 4, 12)
    eu-central-1c = cidrsubnet(var.cidr_block, 4, 13)
  }

  db_port = 5432
}


resource "aws_subnet" "backend_db" {
  count = var.enable_rds_db ? length(local.subnets_db) : 0

  vpc_id                  = aws_vpc.backend.id
  cidr_block              = element(values(local.subnets_db), count.index)
  map_public_ip_on_launch = false

  availability_zone = element(keys(local.subnets_db), count.index)

  tags = merge(
    var.tags,
    {
      "Name" = format("backend-db-%02d", count.index)
    },
  )
}

resource "aws_db_subnet_group" "backend_db" {
  count      = var.enable_rds_db ? 1 : 0
  name       = "backend_db"
  subnet_ids = aws_subnet.backend_db.*.id

  tags = merge(
    var.tags,
    {
      "Name" = "backend-db"
    },
  )
}

resource "aws_db_instance" "backend_db" {
  count             = var.enable_rds_db ? 1 : 0
  identifier        = var.db_instance_identifier
  instance_class    = var.db_instance_size
  allocated_storage = var.db_instance_storage
  storage_type      = "gp2"

  engine         = "postgres"
  engine_version = var.db_engine_version

  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  copy_tags_to_snapshot       = true

  deletion_protection = var.enable_deletion_protection
  skip_final_snapshot = var.enable_deletion_protection ? false : true

  #db details
  port     = local.db_port
  db_name  = var.db_instance_name
  username = var.db_instance_username
  password = var.db_instance_password

  multi_az               = var.db_multi_az #can't choose AZ, have to put backend into same AZ after startup
  publicly_accessible    = false
  db_subnet_group_name   = aws_db_subnet_group.backend_db[0].id
  vpc_security_group_ids = [aws_security_group.backend_db[0].id, module.peering.vpn_peering_security_group_id]

  backup_retention_period = var.db_backup_retention_period #in days
  backup_window           = "00:30-01:30"
  maintenance_window      = "Sat:02:00-Sat:03:00"

  tags = merge(
    var.tags,
    {
      "Name"   = "backend-db",
      "backup" = var.db_backup_tag_value
    },
  )

  lifecycle {
    ignore_changes = [
      password,
    ]
  }
}
